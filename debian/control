Source: iml
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Tobias Hansen <thansen@debian.org>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libgmp-dev,
               libgsl0-dev,
               libblas-dev
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/science-team/iml
Vcs-Git: https://salsa.debian.org/science-team/iml.git
Homepage: https://www.cs.uwaterloo.ca/~astorjoh/iml.html
Rules-Requires-Root: no

Package: libiml0
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Integer Matrix Library, runtime files
 IML is a library for exact, dense linear algebra over the integers.
 IML contains algorithms for nonsingular rational system solving,
 computing the right nullspace of an integer matrix, and certified
 linear system solving.
 .
 In addition, IML provides some low level routines for a variety of
 mod p matrix operations: computing the row-echelon form, determinant,
 rank profile, and inverse of a mod p matrix.  These mod p routines
 are not general purpose; they require that p satisfy some
 preconditions based on the dimension of the input matrix (usually p
 should be prime and should be no more than about 20 bits long).
 .
 This package contains runtime files for IML.

Package: libiml-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libiml0 (= ${binary:Version})
Description: Integer Matrix Library, development files
 IML is a library for exact, dense linear algebra over the integers.
 IML contains algorithms for nonsingular rational system solving,
 computing the right nullspace of an integer matrix, and certified
 linear system solving.
 .
 In addition, IML provides some low level routines for a variety of
 mod p matrix operations: computing the row-echelon form, determinant,
 rank profile, and inverse of a mod p matrix.  These mod p routines
 are not general purpose; they require that p satisfy some
 preconditions based on the dimension of the input matrix (usually p
 should be prime and should be no more than about 20 bits long).
 .
 This package contains development files for IML.
