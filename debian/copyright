Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: IML - Integer Matrix Library
Upstream-Contact: Zhuliang Chen <z4chen@uwaterloo.ca>,
                  Cory Fletcher <c2fletcher@scg.uwaterloo.ca>,
                  Arne Storjohann <astorjoh@scg.uwaterloo.ca>
Source: https://cs.uwaterloo.ca/~astorjoh/iml.html

Files: *
Copyright: (C) 2004-2007 Zhuliang Chen, Arne Storjohann, and Cory Fletcher
License: GPL-2+

Files: debian/*
Copyright: (C) 2008, Tim Abbott <tabbott@mit.edu>
           2013 Tobias Hansen <thansen@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'
